# Thesis Latex Template



## Compilation

```
pdflatex -shell-escape thesis.tex
bibtex thesis.aux
pdflatex -shell-escape thesis.tex
pdflatex -shell-escape thesis.tex
```



## Setup page size

You can modify the page size with these commands:

```latex
\documentclass[twoside,epsf,times]{report}
\usepackage[letterpaper,margin=1in]{geometry}
%\usepackage{showframe}
```

By default, we use `letterpaper` as page size, and use a margin of `1in`. You can uncomment the `showframe`package to see the margins. For more information about the `geometry`package, check these resources:

- [Page size and margins](https://www.overleaf.com/learn/latex/Page_size_and_margins)
- [Page layout](https://en.wikibooks.org/wiki/LaTeX/Page_Layout)



## Dedication page

You can add a dedication using this block:

```latex
% ------------------------------
% dedication
% ------------------------------

\begin{center}
    \thispagestyle{empty}
    \vspace*{\fill}
    To my special other.
    \vspace*{\fill}
\end{center}
```

Inspired from [this](https://tex.stackexchange.com/questions/319875/how-to-make-dedication-page-and-move-words-to-the-middle) and [this](https://tex.stackexchange.com/questions/102830/dedication-page-in-article-class). We use two commands: `\thispagestyle` and `\vspace`. From [this post](http://www.personal.ceu.hu/tex/pagestyl.htm#thispgstyle):

`\pagestyle{option}`: This command changes the style from the current page on throughout the remainder of the document. The valid options are:

| Option       | Description                                                  |
| ------------ | ------------------------------------------------------------ |
| `plain`      | Just a plain page number.                                    |
| `empty`      | No header, no footer, no page number.                        |
| `headings`   | Include the header.                                          |
| `myheadings` | Include the header specified by the `\markboth` and `\markright` commands. |



**todo** Explain `\vspace*`



## Table of contents

You can create the table of contents with this command:

```latex
\tableofcontents
```



## List of figures, tables, and algorithms

You can use the following commands to create the list of figures, tables, and algorithms:

```latex
\listoffigures
\listoftables
\listofalgorithms  % requires the algorithm and algpseudocode packages
```



## List of abbreviations

Required packages:

```latex
\usepackage{enumitem}
\setlist{nosep}			% to reduce the space between items (i.e., abbreviations)
```

You can then add the abbreviations as follows:

```latex
\chapter*{List of Abbreviations}

\begin{description}[labelwidth=1in,labelsep=4em,align=left]
    \item[NN]       Neural network 
    \item[CNN]      Convolutional neural network 
    \item[LSTM]     Long-short term memory 
\end{description}
```

More information [here](https://tex.stackexchange.com/questions/126221/spacing-between-item-and-description-using-description-list) and [here](https://tex.stackexchange.com/questions/10684/vertical-space-in-lists).





## Changelog

- `40ce816` This version compiles with no relevant warnings.

```
bibtex thesis.aux
-----------------

This is BibTeX, Version 0.99d (TeX Live 2015/Debian)
The top-level auxiliary file: thesis.aux
The style file: plain.bst
Database file #1: bibliography.bib
```

```
pdflatex -shell-escape thesis.tex 
---------------------------------

This is pdfTeX, Version 3.14159265-2.6-1.40.16 (TeX Live 2015/Debian) (preloaded format=pdflatex)
 \write18 enabled.
entering extended mode
(./thesis.tex
LaTeX2e <2016/02/01>
Babel <3.9q> and hyphenation patterns for 8 language(s) loaded.
(/usr/share/texlive/texmf-dist/tex/latex/base/report.cls
Document Class: report 2014/09/29 v1.4h Standard LaTeX document class
(/usr/share/texlive/texmf-dist/tex/latex/base/size10.clo))

LaTeX Warning: Unused global option(s):
    [epsf,times].

(./thesis.aux) [1{/var/lib/texmf/fonts/map/pdftex/updmap/pdftex.map}]
(./thesis.toc) [1] [2] [3]
Chapter 1.
(./thesis.bbl [4]) [5] (./thesis.aux) )</usr/share/texlive/texmf-dist/fonts/typ
e1/public/amsfonts/cm/cmbx10.pfb></usr/share/texlive/texmf-dist/fonts/type1/pub
lic/amsfonts/cm/cmbx12.pfb></usr/share/texlive/texmf-dist/fonts/type1/public/am
sfonts/cm/cmr10.pfb></usr/share/texlive/texmf-dist/fonts/type1/public/amsfonts/
cm/cmr12.pfb></usr/share/texlive/texmf-dist/fonts/type1/public/amsfonts/cm/cmti
10.pfb>
Output written on thesis.pdf (6 pages, 71031 bytes).
Transcript written on thesis.log.
```

- `fec2e9d` This version has a good page size.





# Enfoque

- Definir todas las variables al inicio del documento
- Crear las intro pages en `thesis.tex`, una por una
- La hoja de las firmas la haremos fija
- La hoja de los agradecimientos la haremos fija
- Agregar mas detalles en abstract, quiza se vea bien
- La hoja de los nomenclaruta la haremos fija



## Paginas fijas

- [x] Dedicatoria
- [x] Firmas
- [x] Agradecimientos
- [x] Nomenclatura
- [x] Copyright



# TODO

- [x] Verificar que la lista de figuras/tablas/demas/abstract dejen una hoja vacia
- [ ] Verificar que cada capitulo comience en una hoja nueva



## Contact

Auraham Camacho `auraham.cg@gmail.com`